import { IsEmail, IsString } from 'class-validator';

class UserLoginDto {
	@IsEmail({}, { message: 'Неверно указан email' })
	email: string;

	@IsString({ message: 'Не указан пароль' })
	password: string;
}

export { UserLoginDto };
