import { UserModel } from '@prisma/client';

import { UserRegisterDto } from '../dto/user-register.dto';
import { UserLoginDto } from '../dto/user-login.dto';

interface IUsersService {
	create: (user: UserRegisterDto) => Promise<UserModel | null>;
	validate: (user: UserLoginDto) => Promise<boolean>;
}

export type { IUsersService };
