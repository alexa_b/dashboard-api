import { inject, injectable } from 'inversify';
import { UserModel } from '@prisma/client';

import { UserLoginDto } from '../dto/user-login.dto';
import { UserRegisterDto } from '../dto/user-register.dto';
import { UserEntity } from '../entity/user.entity';
import { IUsersService } from './users.service.interface';
import { TYPES } from '../../types';
import { IConfigService, KEYS_CONFIG } from '../../config/config.service.interface';
import { IUsersRepository } from '../repository/users.repository.interface';

@injectable()
class UserService implements IUsersService {
	constructor(
		@inject(TYPES.ConfigService) private configService: IConfigService,
		@inject(TYPES.UsersRepository) private usersRepository: IUsersRepository,
	) {}

	async create({ email, name, password }: UserRegisterDto): Promise<UserModel | null> {
		const existedUser = await this.usersRepository.find(email);
		if (existedUser) {
			return null;
		}

		const newUser = new UserEntity(email, name);
		const salt = this.configService.get<number>(KEYS_CONFIG.salt);

		await newUser.setPassword(password, salt);
		return this.usersRepository.create(newUser);
	}

	async validate({ email, password }: UserLoginDto): Promise<boolean> {
		const existedUser = await this.usersRepository.find(email);
		if (!existedUser) {
			return false;
		}

		const newUser = new UserEntity(existedUser.email, existedUser.name, existedUser.password);
		return newUser.comparePassword(password);
	}
}

export { UserService };
