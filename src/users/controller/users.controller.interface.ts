import { Request, NextFunction, Response } from 'express';
import { BaseController } from '../../common/controller/base.controller';
import { HookExpress } from '../../types';

interface IUsersController extends BaseController {
	login: HookExpress;
	register: HookExpress;
}

export type { IUsersController };
