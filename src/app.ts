import express, { Express } from 'express';
import { Server } from 'http';
import { inject, injectable } from 'inversify';

import { ILogger } from './logger/logger.interface';
import { TYPES } from './types';
import { IExeptionFilter } from './errors/exeption.filter.interface';
import { IUsersController } from './users/controller/users.controller.interface';
import { IConfigService } from './config/config.service.interface';
import { PrismaService } from './common/database/priasma.service';

/**
 * @class
 * Основа приложения
 */
@injectable()
class App {
	app: Express;
	server: Server;
	port: number;

	constructor(
		@inject(TYPES.ILogger) private logger: ILogger,
		@inject(TYPES.UsersController) private userController: IUsersController,
		@inject(TYPES.ExeptionFilter) private exeptionFilter: IExeptionFilter,
		@inject(TYPES.PrismaService) private prismaService: PrismaService,
	) {
		this.app = express();
		this.port = 8000;
	}

	private useMiddleware(): void {
		this.app.use(express.json());
	}

	private useRoutes(): void {
		this.app.use('/users', this.userController.router);
	}

	private useExeptionFilters(): void {
		this.app.use(this.exeptionFilter.catch.bind(this.exeptionFilter));
	}

	async init(): Promise<void> {
		this.useMiddleware();
		this.useRoutes();
		this.useExeptionFilters();

		await this.prismaService.connect();

		this.server = this.app.listen(this.port);
		this.logger.log(`Сервер запущен на http://localhost:${this.port}`);
	}
}

export { App };
