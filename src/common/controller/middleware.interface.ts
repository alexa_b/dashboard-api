import { HookExpress } from '../../types';

interface IMiddleware {
	execute: HookExpress;
}

export { IMiddleware };
