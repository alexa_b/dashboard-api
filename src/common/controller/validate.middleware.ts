import { ClassConstructor, plainToInstance } from 'class-transformer';
import { validate } from 'class-validator';

import { HookExpress } from '../../types';
import { IMiddleware } from './middleware.interface';

class ValidateMiddleware implements IMiddleware {
	constructor(private classToValidate: ClassConstructor<object>) {}

	execute: HookExpress = ({ body }, res, next) => {
		const instance = plainToInstance(this.classToValidate, body);
		validate(instance).then((errors) => {
			if (errors.length) {
				res.status(422).send(errors);
			} else {
				next();
			}
		});
	};
}

export { ValidateMiddleware };
