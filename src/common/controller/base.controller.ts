import { Router, Response } from 'express';
import { injectable } from 'inversify';

import { ExpressReturnType, IControllerRoute } from './route.interface';
import { ILogger } from '../../logger/logger.interface';

/**
 * @class
 * Базовый контрлоллер
 */
@injectable()
abstract class BaseController {
	private readonly _router: Router;

	constructor(private logger: ILogger) {
		this._router = Router();
	}

	get router(): Router {
		return this._router;
	}

	public send<T>(res: Response, code: number, message: T): ExpressReturnType {
		res.type('application/json');
		return res.status(code).json(message);
	}

	public ok<T>(res: Response, message: T): ExpressReturnType {
		return this.send<T>(res, 200, message);
	}

	public created(res: Response): ExpressReturnType {
		return res.sendStatus(201);
	}

	protected bindRoutes(routes: IControllerRoute[]): void {
		routes.forEach(({ method, path, func, middlewares }) => {
			this.logger.log(`[${method}] ${path}`);

			const middlewareHooks = middlewares?.map((m) => m.execute.bind(m));
			const handler = func.bind(this);
			const pipline = middlewareHooks ? [...middlewareHooks, handler] : handler;

			this.router[method](path, pipline);
		});
	}
}

export { BaseController };
