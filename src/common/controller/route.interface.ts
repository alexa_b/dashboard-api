import { Response, Router } from 'express';
import { IMiddleware } from './middleware.interface';
import { HookExpress } from '../../types';

/** Роут */
interface IControllerRoute {
	/** Путь */
	path: string;

	/** Cb */
	func: HookExpress;

	/** Тип метода */
	method: keyof Pick<Router, 'get' | 'post' | 'delete' | 'put'>;

	middlewares?: IMiddleware[];
}

type ExpressReturnType = Response<any, Record<string, any>>;

export type { IControllerRoute, ExpressReturnType };
