import { config, DotenvConfigOutput } from 'dotenv';
import { inject, injectable } from 'inversify';

import { IConfigService } from './config.service.interface';
import { TYPES } from '../types';
import { ILogger } from '../logger/logger.interface';

@injectable()
class ConfigService implements IConfigService {
	private config: DotenvConfigOutput;

	constructor(@inject(TYPES.ILogger) private logger: ILogger) {
		const result: DotenvConfigOutput = config();
		if (result.error) {
			this.logger.error('[ConfigService] Не удалось прочитать файл .env или он отсутсвует');
		} else {
			this.logger.log('[ConfigService] Кoнфигурация загружена');
			this.config = result.parsed!;
		}
	}

	get<T extends string | number>(key: string): T {
		const value = this.config[key as keyof typeof this.config] as unknown as T;
		return isNaN(Number(value)) ? value : (Number(value) as unknown as T);
	}
}

export { ConfigService };
