interface IConfigService {
	get: <T extends string | number>(key: string) => T;
}

const KEYS_CONFIG = {
	salt: 'SALT',
} as const;

export { type IConfigService, KEYS_CONFIG };
