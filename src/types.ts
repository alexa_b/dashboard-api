import { NextFunction, Request, Response } from 'express';

const TYPES = {
	Application: Symbol.for('Application'),
	ILogger: Symbol.for('ILogger'),
	UsersController: Symbol.for('UsersController'),
	UsersService: Symbol.for('UsersService'),
	UsersRepository: Symbol.for('UsersRepository'),
	ExeptionFilter: Symbol.for('ExeptionFilter'),
	ConfigService: Symbol.for('ConfigService'),
	PrismaService: Symbol.for('PrismaService'),
};

type HookExpress = (req: Request, res: Response, next: NextFunction) => void;

export { TYPES, HookExpress };
