import { Container, ContainerModule, interfaces } from 'inversify';
import 'reflect-metadata';

import { App } from './app';
import { ExeptionFilter } from './errors/exeption.filter';
import { IExeptionFilter } from './errors/exeption.filter.interface';
import { LoggerService } from './logger/logger.service';
import { ILogger } from './logger/logger.interface';
import { UsersController } from './users/controller/users.controller';
import { TYPES } from './types';
import { IUsersController } from './users/controller/users.controller.interface';
import { IUsersService } from './users/services/users.service.interface';
import { UserService } from './users/services/users.service';
import { IConfigService } from './config/config.service.interface';
import { ConfigService } from './config/config.service';
import { PrismaService } from './common/database/priasma.service';
import { IUsersRepository } from './users/repository/users.repository.interface';
import { UsersRepository } from './users/repository/users.repository';

interface IBootstrapReturn {
	app: App;
	appContainer: Container;
}

const appBindings = new ContainerModule((bind: interfaces.Bind) => {
	/** app */
	bind<App>(TYPES.Application).to(App);

	/** common */
	bind<ILogger>(TYPES.ILogger).to(LoggerService).inSingletonScope();
	bind<IExeptionFilter>(TYPES.ExeptionFilter).to(ExeptionFilter).inSingletonScope();

	/** controllers */
	bind<IUsersController>(TYPES.UsersController).to(UsersController);

	/** services */
	bind<IUsersService>(TYPES.UsersService).to(UserService);
	bind<IConfigService>(TYPES.ConfigService).to(ConfigService).inSingletonScope();
	bind<PrismaService>(TYPES.PrismaService).to(PrismaService).inSingletonScope();

	/** repository */
	bind<IUsersRepository>(TYPES.UsersRepository).to(UsersRepository).inSingletonScope();
});

const bootstrap = (): IBootstrapReturn => {
	const appContainer = new Container();
	appContainer.load(appBindings);

	const app = appContainer.get<App>(TYPES.Application);
	app.init();

	return {
		app,
		appContainer,
	};
};

export const { app, appContainer } = bootstrap();

export { appBindings, IBootstrapReturn };
